using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ConsultoriasApi.Models;

namespace ConsultoriasApi.Data
{
    public interface IRepository<T> where T : IModel
    {
        Task<T> GetOneAsync(int id);
        Task<IList<T>> GetManyAsync(Expression<Func<T, bool>> predicate = null);
        Task<T> InsertAsync(T item);
    }
}