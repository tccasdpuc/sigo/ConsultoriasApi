using Microsoft.EntityFrameworkCore;

namespace ConsultoriasApi.Data
{
    public class DataContextFactory : IDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            optionsBuilder.UseInMemoryDatabase("DataContext");
            return new DataContext(optionsBuilder.Options);
        }
    }
}