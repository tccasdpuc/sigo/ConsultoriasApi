using Microsoft.EntityFrameworkCore;

namespace ConsultoriasApi.Data
{
    public interface IDbContextFactory<out TContext> where TContext : DbContext
    {
        public TContext CreateDbContext();
    }
}