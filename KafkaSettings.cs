namespace ConsultoriasApi
{
    public class KafkaSettings
    {
        public const string SettingsKey = "Kafka";
        public string BootstrapServer { get; set; }
    }
}