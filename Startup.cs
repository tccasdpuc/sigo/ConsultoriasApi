using System;
using ConsultoriasApi.Data;
using ConsultoriasApi.Events;
using ConsultoriasApi.Kafka;
using ConsultoriasApi.Models;
using ConsultoriasApi.Service;
using ContratosApi.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;

namespace ConsultoriasApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KafkaSettings>(Configuration.GetSection(KafkaSettings.SettingsKey));
            services.PostConfigure<KafkaSettings>(KafkaSettingsPostConfigure);

            services.AddSingleton<IEventMediator, EventMediator>();

            services.AddTransient<IDbContextFactory<DataContext>, DataContextFactory>();
            services.AddTransient<IRepository<Consultoria>, ConsultoriaService>();
            services.AddTransient<IRepository<Contrato>, ContratoService>();
            services.AddTransient<IRepository<Empresa>, EmpresaService>();

            services.AddHostedService<KafkaProducer>();

            services.AddDiscoveryClient(Configuration);
            PostConfigureEureka(services);

            services.AddControllers();
            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseDiscoveryClient();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "SIGO Consultorias API"); });
        }

        private void KafkaSettingsPostConfigure(KafkaSettings settings)
        {
            var envBootstrapServer = Environment.GetEnvironmentVariable("KAFKA_BOOTSTRAP_SERVER");
            if (string.IsNullOrEmpty(envBootstrapServer) == false)
            {
                settings.BootstrapServer = envBootstrapServer;
            }
        }

        private void PostConfigureEureka(IServiceCollection services)
        {

            services.PostConfigure<EurekaClientOptions>(client =>
            {
                var envEurekaServiceUrl = Environment.GetEnvironmentVariable("EUREKA_SERVICE_URL");
                if (string.IsNullOrEmpty(envEurekaServiceUrl) == false
                    && Uri.TryCreate(envEurekaServiceUrl, UriKind.Absolute, out var url))
                {
                    client.ServiceUrl = url.ToString();
                }
            });

            services.PostConfigure<EurekaInstanceOptions>(instance =>
            {
                var envPort = Environment.GetEnvironmentVariable("PORT");
                if (string.IsNullOrEmpty(envPort) == false && ushort.TryParse(envPort, out var port))
                {
                    instance.Port = port;
                    instance.InstanceId = $"{instance.HostName}:{instance.AppName}:{instance.Port}".ToLower();
                }
            });

        }
    }
}