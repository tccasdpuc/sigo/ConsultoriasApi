using ConsultoriasApi.Models;

namespace ConsultoriasApi.Events
{
    public delegate void OnConsultoriaHandler(Consultoria consultoria);

    public interface IEventMediator
    {
        public event OnConsultoriaHandler OnConsultoria;
        public void TriggerOnConsultoria(Consultoria consultoria);
    }

    public class EventMediator : IEventMediator
    {
        public event OnConsultoriaHandler OnConsultoria;

        public void TriggerOnConsultoria(Consultoria consultoria)
        {
            OnConsultoria?.Invoke(consultoria);
        }
    }
}