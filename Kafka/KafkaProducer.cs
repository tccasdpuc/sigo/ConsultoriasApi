using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using ConsultoriasApi.Events;
using ConsultoriasApi.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ConsultoriasApi.Kafka
{
    internal class JsonKafkaSerializer<T> : ISerializer<T>
    {
        public byte[] Serialize(T data, SerializationContext context)
        {
            var str = JsonSerializer.Serialize(data);
            return Encoding.UTF8.GetBytes(str);
        }
    }

    public class KafkaProducer : IHostedService, IDisposable
    {
        private readonly OnConsultoriaHandler _consultoriaHandler;
        private readonly IEventMediator _eventMediator;
        private readonly object _lock;
        private readonly ILogger<KafkaProducer> _logger;
        private readonly IProducer<Null, Consultoria> _producer;
        private readonly List<Task> _taskList;

        public KafkaProducer(IOptions<KafkaSettings> options, IEventMediator eventMediator,
            ILogger<KafkaProducer> logger)
        {
            _eventMediator = eventMediator;
            _logger = logger;
            _consultoriaHandler = OnConsultoriaHandler;
            _taskList = new List<Task>();
            _lock = new object();

            var config = new ProducerConfig { BootstrapServers = options.Value.BootstrapServer };
            _producer = new ProducerBuilder<Null, Consultoria>(config)
                .SetValueSerializer(new JsonKafkaSerializer<Consultoria>())
                .Build();
        }

        public void Dispose()
        {
            _producer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _eventMediator.OnConsultoria += _consultoriaHandler;
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _eventMediator.OnConsultoria -= _consultoriaHandler;
            IList<Task> tasks;
            lock (_lock)
            {
                _taskList.RemoveAll(t => t.IsCompleted);
                tasks = _taskList;
            }

            await Task.WhenAll(tasks);
        }

        private void OnConsultoriaHandler(Consultoria consultoria)
        {
            lock (_lock)
            {
                _taskList.RemoveAll(t => t.IsCompleted);
                var task = ProduceAsync(consultoria);
                _taskList.Add(task);
            }
        }

        private async Task ProduceAsync(Consultoria consultoria)
        {
            if (consultoria == null) return;
            try
            {
                var msg = new Message<Null, Consultoria> { Value = consultoria };
                var dr = await _producer.ProduceAsync("consultoria", msg);
                _logger.LogDebug($"Delivered {dr.Value.Id} to {dr.TopicPartitionOffset}");
            }
            catch (ProduceException<Null, Consultoria> e)
            {
                _logger.LogError(e, "ProduceAsyncError");
            }
        }
    }
}