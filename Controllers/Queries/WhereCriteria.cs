using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ConsultoriasApi.Controllers.Queries
{
    public static class Extensions
    {
        public static Expression<Func<T, bool>> ToExpression<T>(this IReadOnlyCollection<WhereCriteria<T>> criteriaList)
            where T : class
        {
            if (criteriaList.Count == 0) return null;
            var param = Expression.Parameter(typeof(T), "i");
            Expression ex = null;
            foreach (var whereCriteria in criteriaList)
            {
                var e = whereCriteria.ToExpression(param);
                ex = ex == null ? e : Expression.And(ex, e);
            }

            return ex != null ? Expression.Lambda<Func<T, bool>>(ex, param) : null;
        }

        public static PropertyInfo GetPropertyInfo(this Type type, string name)
        {
            var properties = type.GetProperties();
            var propertyInfo = properties.FirstOrDefault(info =>
                string.Equals(info.Name.ToUpper(), name.ToUpper(), StringComparison.Ordinal));
            if (propertyInfo == null) throw new ArgumentException($"Invalid field '{name}'");

            return propertyInfo;
        }
    }

    public class WhereCriteria<T> where T : class
    {
        private string _field;

        public WhereCriteria(string field, string op, params string[] values)
            : this(field, WhereOperator.Parse(op), values)
        {
        }

        public WhereCriteria(string field, WhereOperator op, params string[] values)
        {
            Field = field;
            Operator = op;
            Values = values;
        }

        public string Field
        {
            get => _field;
            set
            {
                var propertyInfo = typeof(T).GetPropertyInfo(value);
                _field = propertyInfo.Name;
                FieldType = propertyInfo.PropertyType;
            }
        }

        public Type FieldType { get; private set; }

        public WhereOperator Operator { get; }

        public string[] Values { get; }

        /// <summary>
        ///     Parse from "field,op,value". Ex: "name,=,Jon"
        /// </summary>
        /// <exception cref="Exception"></exception>
        public static WhereCriteria<TM> Parse<TM>(string where) where TM : class
        {
            var criteria = where.Split(",");
            if (criteria.Length < 3) throw new ArgumentException("Invalid criteria");

            var field = criteria[0];
            var op = criteria[1];
            var valuesLength = criteria.Length - 2;
            var values = new string[valuesLength];
            Array.Copy(criteria, 2, values, 0, valuesLength);
            return new WhereCriteria<TM>(field, op, values);
        }

        public Expression ToExpression(ParameterExpression param)
        {
            var left = Expression.PropertyOrField(param, Field);
            var valueConverter = TypeDescriptor.GetConverter(FieldType);
            var value = valueConverter.ConvertFrom(Values[0]);
            var right = Expression.Constant(value, FieldType);

            if (Operator == WhereOperator.Eq) return Expression.Equal(left, right);

            if (Operator == WhereOperator.Ne) return Expression.NotEqual(left, right);

            if (Operator == WhereOperator.Gt) return Expression.GreaterThan(left, right);

            if (Operator == WhereOperator.Lt) return Expression.LessThan(left, right);

            if (Operator == WhereOperator.Ge) return Expression.GreaterThanOrEqual(left, right);

            if (Operator == WhereOperator.Le) return Expression.LessThanOrEqual(left, right);

            if (Operator == WhereOperator.Lk)
                if (FieldType == typeof(string))
                {
                    var method = typeof(string).GetMethod("Contains", new[] {typeof(string)});
                    return method == null ? null : Expression.Call(left, method, right);
                }

            if (Operator == WhereOperator.In)
            {
                Expression ex = null;
                foreach (var val in Values)
                {
                    var v = valueConverter.ConvertFrom(val);
                    var r = Expression.Constant(v, FieldType);
                    var e = Expression.Equal(left, r);
                    ex = ex == null ? e : Expression.Or(ex, e);
                }

                return ex;
            }

            return null;
        }
    }
}