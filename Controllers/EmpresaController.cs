using ConsultoriasApi.Data;
using ConsultoriasApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ConsultoriasApi.Controllers
{
    [ApiController]
    [Route("empresa")]
    public class EmpresaController : BaseController<Empresa>
    {
        public EmpresaController(IRepository<Empresa> repository) : base(repository)
        {
        }
    }
}