using ConsultoriasApi.Data;
using ConsultoriasApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ConsultoriasApi.Controllers
{
    [ApiController]
    [Route("consultoria")]
    public class ConsultoriaController : BaseController<Consultoria>
    {
        public ConsultoriaController(IRepository<Consultoria> repository) : base(repository)
        {
        }
    }
}