using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConsultoriasApi.Controllers.Queries;
using ConsultoriasApi.Data;
using ConsultoriasApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ConsultoriasApi.Controllers
{
    [Produces("application/json")]
    public abstract class BaseController<T> : ControllerBase where T : class, IModel
    {
        private readonly IRepository<T> _repository;

        protected BaseController(IRepository<T> repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public virtual Task<T> GetById(int id)
        {
            return _repository.GetOneAsync(id);
        }

        [HttpGet("list")]
        public virtual async Task<IList<T>> GetList([FromQuery(Name = "where[]")] string[] where)
        {
            var criteriaList = Array.ConvertAll(where, WhereCriteria<T>.Parse<T>);
            var predicate = criteriaList.ToExpression();
            var result = await _repository.GetManyAsync(predicate);
            return result;
        }

        [HttpPost]
        public virtual async Task<IActionResult> Insert([FromBody] T model)
        {
            var result = await _repository.InsertAsync(model);
            return new JsonResult(result);
        }
    }
}