using ConsultoriasApi.Controllers;
using ConsultoriasApi.Data;
using ConsultoriasApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace ConsultoriasApi.Controllers
{
    [ApiController]
    [Route("contrato")]
    public class ContratoController : BaseController<Contrato>
    {
        public ContratoController(IRepository<Contrato> repository) : base(repository)
        {
        }
    }
}