using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsultoriasApi.Models
{
    public class Contrato : IModel
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Descricao { get; set; }
        public int EmpresaId { get; set; }
    }
}