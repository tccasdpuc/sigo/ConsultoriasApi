namespace ConsultoriasApi.Models
{
    public interface IModel
    {
        int Id { get; set; }
    }
}