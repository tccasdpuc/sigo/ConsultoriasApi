using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsultoriasApi.Models
{
    public class Consultoria : IModel
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }
        public string Descricao { get; set; }
        public int ContratoId { get; set; }
        public DateTime Data { get; set; }
    }
}