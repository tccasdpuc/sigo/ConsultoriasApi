using System.Threading.Tasks;
using ConsultoriasApi.Data;
using ConsultoriasApi.Events;
using ConsultoriasApi.Models;

namespace ConsultoriasApi.Service
{
    public class ConsultoriaService : BaseService<DataContext, Consultoria>
    {
        private readonly IEventMediator _eventMediator;

        public ConsultoriaService(IDbContextFactory<DataContext> contextFactory, IEventMediator eventMediator)
            : base(contextFactory, c => c.Consultorias)
        {
            _eventMediator = eventMediator;
        }

        public override async Task<Consultoria> InsertAsync(Consultoria item)
        {
            var result = await base.InsertAsync(item);
            _eventMediator.TriggerOnConsultoria(result);
            return result;
        }
    }
}