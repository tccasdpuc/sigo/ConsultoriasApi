using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ConsultoriasApi.Data;
using ConsultoriasApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ConsultoriasApi.Service
{
    public abstract class BaseService<TContext, T> : IRepository<T> where T : class, IModel where TContext : DbContext
    {
        private readonly IDbContextFactory<TContext> _contextFactory;
        private readonly Func<TContext, DbSet<T>> _dbSetGetter;

        protected BaseService(IDbContextFactory<TContext> contextFactory, Func<TContext, DbSet<T>> dbSetGetter)
        {
            _contextFactory = contextFactory;
            _dbSetGetter = dbSetGetter;
        }

        public virtual async Task<T> GetOneAsync(int id)
        {
            await using var context = _contextFactory.CreateDbContext();
            IQueryable<T> queryable = _dbSetGetter.Invoke(context);
            var result = await queryable.FirstOrDefaultAsync(i => i.Id == id);
            return result;
        }

        public virtual async Task<IList<T>> GetManyAsync(Expression<Func<T, bool>> predicate = null)
        {
            await using var context = _contextFactory.CreateDbContext();
            IQueryable<T> queryable = _dbSetGetter.Invoke(context);
            if (predicate != null)
            {
                queryable = queryable.Where(predicate);
            }

            var result = await queryable.ToListAsync();
            return result;
        }

        public virtual async Task<T> InsertAsync(T item)
        {
            await using var context = _contextFactory.CreateDbContext();
            var dbSet = _dbSetGetter.Invoke(context);
            await dbSet.AddAsync(item);
            await context.SaveChangesAsync();
            return item;
        }
    }
}