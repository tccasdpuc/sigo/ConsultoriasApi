using ConsultoriasApi.Data;
using ConsultoriasApi.Models;

namespace ConsultoriasApi.Service
{
    public class EmpresaService : BaseService<DataContext, Empresa>
    {
        public EmpresaService(IDbContextFactory<DataContext> contextFactory)
            : base(contextFactory, c => c.Empresas)
        {
        }
    }
}