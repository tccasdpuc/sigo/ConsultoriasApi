using ConsultoriasApi.Data;
using ConsultoriasApi.Models;
using ConsultoriasApi.Service;

namespace ContratosApi.Service
{
    public class ContratoService : BaseService<DataContext, Contrato>
    {
        public ContratoService(IDbContextFactory<DataContext> contextFactory)
            : base(contextFactory, c => c.Contratos)
        {
        }
    }
}